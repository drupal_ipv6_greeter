This module allows you to show a greeter block to the users accessing the site
over IPv6, or to inform users on IPv4 that they are not using IPv6 yet.

This version of the module only works with Drupal 6.x.

The icons used in the blocks are taken from:
http://commons.wikimedia.org/wiki/File:Commons-emblem-success.svg
http://commons.wikimedia.org/wiki/File:Commons-emblem-hand-orange.svg


Copyright (C) 2011  Antonio Ospite <ospite@studenti.unina.it>
http://ao2.it
